package com.pixellato.bumblebee;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class EditActivity extends AppCompatActivity {

    Bitmap mainPhoto, copyPhoto;
    Uri mainUri, copyUri, copyxUri;
    boolean changed = false;
    int clickCount = 0;
    static final int REQUEST_IMAGE_ADJUST = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        clickCount = 0;
        mainPhoto = ((SharedData) EditActivity.this.getApplication()).getSomeBitmap();
        mainUri =  getBareImageUri(EditActivity.this, mainPhoto);

        ImageView imageView = (ImageView) findViewById(R.id.effectsview2);
        imageView.setImageBitmap(mainPhoto);

        copyPhoto = mainPhoto;
        copyUri = mainUri;
        String s = ((SharedData) EditActivity.this.getApplication()).getSomeVariable();
    }


    public void handleFlip(View view){
        Bitmap bMap = copyPhoto;
        changed = true;

        float cx = bMap.getWidth()/2f;
        float cy = bMap.getHeight()/2f;

        Matrix matrix = new Matrix();
        matrix.postScale(-1, 1, cx, cy);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bMap,bMap.getWidth(),bMap.getHeight(),true);
        Bitmap flippedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        copyPhoto = flippedBitmap;
        ImageView mv = (ImageView) findViewById(R.id.effectsview2);
        mv.setImageBitmap(copyPhoto);
        Log.i("fc", "grown");
    }

    public void handleRotate(View view){
        Bitmap bMap = copyPhoto;
        changed = true;

        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bMap,bMap.getWidth(),bMap.getHeight(),true);
        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        copyPhoto = rotatedBitmap;
        ImageView mv = (ImageView) findViewById(R.id.effectsview2);
        mv.setImageBitmap(copyPhoto);
        Log.i("fc", "sys");
    }

    public void handleCrop(View view){
        changed = true;
        try{

            copyxUri = Uri.fromFile(new File(EditActivity.this.getExternalCacheDir().getPath(), "cropResult.jpg"));
            copyUri = getBareImageUri(EditActivity.this, copyPhoto);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(copyUri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("scale", true);
            cropIntent.putExtra("return-data", true);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, copyxUri);
            cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.PNG.toString());

            startActivityForResult(cropIntent, REQUEST_IMAGE_ADJUST);

        }catch (ActivityNotFoundException anfe) {
            Log.i("EXCEPTION:", "Activity Not Found Exception!");
            String errorMessage = "Your device doesn't support crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }catch (Exception ex) {
            Log.i("ERR:", "Exception found in handleEditClick!");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_ADJUST && resultCode == RESULT_OK){

            File f = new File(copyxUri.getPath());
            Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());

            copyPhoto = bmp;
            ImageView mv = (ImageView) findViewById(R.id.effectsview2);
            mv.setImageBitmap(copyPhoto);

        }
    }

    public Uri getBareImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void handleDone(View view){
        ((SharedData) EditActivity.this.getApplication()).setSomeBitmap(copyPhoto);
        Intent myIntent = new Intent(EditActivity.this, FiltersActivity.class);
        EditActivity.this.startActivity(myIntent);
    }


    public Uri saveImage(Bitmap nowPhoto, int x) {

        Bitmap icon = nowPhoto;
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        if (x == 0) {

            boolean bool = shouldAskPermission();
            if (bool) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }
        } else {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Bumble");
            myDir.mkdirs();

            String fname = timeStamp + "-edt-saved.jpg";
            File f = new File (myDir, fname);
            if (f.exists ()) f.delete ();

             try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                icon.compress(Bitmap.CompressFormat.PNG, 100, fo);
                fo.flush();
                fo.close();
             } catch (IOException e) {
                e.printStackTrace();
             }

        }

        return Uri.parse("file:///sdcard/Bumble/"+timeStamp+"-edt-saved.jpg");
    }

    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        if(permsRequestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                System.out.println("NOTE: Saving Image Locally!");
                saveImage(mainPhoto, 1);

            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(changed == true) {
            (new AlertDialog.Builder(this))
                    .setTitle("Back")
                    .setMessage("The edited image will be dismissed. Make sure to save it first.")
                    .setPositiveButton("BACK TO HOMEPAGE", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent myIntent = new Intent(EditActivity.this, MainActivity.class);
                            myIntent.putExtra("sourceAct", "EditActivity");
                            myIntent.putExtra("finUri", mainUri.toString());
                            EditActivity.this.startActivity(myIntent);
                        }
                    })
                    .setNegativeButton("STAY HERE", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } else {

            // here should be another alert dialog box showing (the selected image will be unselected)
            Intent myIntent = new Intent(EditActivity.this, MainActivity.class);
            myIntent.putExtra("sourceAct", "EditActivity");
            myIntent.putExtra("finUri", mainUri.toString());
            EditActivity.this.startActivity(myIntent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        String hiddenMsg = ((SharedData) EditActivity.this.getApplication()).getSomeVariable();
        if(hiddenMsg != null){
            MenuItem alertButton = menu.findItem(R.id.action_alert);
            alertButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_alert) {
            String hiddenMsg = ((SharedData) EditActivity.this.getApplication()).getSomeVariable();
            clickCount++;
            if(true){
                Toast toast=Toast.makeText(getApplicationContext(), hiddenMsg, Toast.LENGTH_SHORT);
                toast.show();
                clickCount = 0;
            }
            return true;
        }

        if (id == R.id.action_save) {
            Toast toast=Toast.makeText(getApplicationContext(),"Saving...",Toast.LENGTH_SHORT);
            toast.show();

            saveImage(copyPhoto, 1);
            toast.cancel();

            Toast toastSaved=Toast.makeText(getApplicationContext(),"Image saved in folder /Bumble.",Toast.LENGTH_SHORT);
            toastSaved.show();
            return true;
        }

        if (id == R.id.action_cancel) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
