package com.pixellato.bumblebee;
import android.app.Application;
import android.graphics.Bitmap;

public class SharedData extends Application {
    private String hiddenMessage;
    public String getSomeVariable(){ return hiddenMessage; }
    public void setSomeVariable(String hiddenMessage){ this.hiddenMessage = hiddenMessage; }

    private Bitmap masterBitmap = null;
    public Bitmap getSomeBitmap(){ return masterBitmap; }
    public void setSomeBitmap(Bitmap masterBitmap){ this.masterBitmap = masterBitmap; }
}
