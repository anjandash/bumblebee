package com.pixellato.bumblebee;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.Manifest;


public class MainActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_SELECT = 2;

    Bitmap mainPhoto, jinxPhoto;
    Uri mainUri;
    Dialog dialog;
    InputStream inputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((SharedData) MainActivity.this.getApplication()).setSomeVariable(null);
        ((SharedData) MainActivity.this.getApplication()).setSomeBitmap(null);

        Button photoButton = (Button) findViewById(R.id.photoButton);
        if(!hasCamera()){ photoButton.setEnabled(false); }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
            System.out.println("PERMISSION PASSED!");
        }
    }

    private boolean hasCamera(){
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera(View view){

        Boolean ch = checkPermissionREAD_EXTERNAL_STORAGE(this);
        if (ch) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Context context = MainActivity.this;
            mainUri = Uri.fromFile(new File(context.getExternalCacheDir().getPath(), "pickImageResult.jpg"));

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mainUri);
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void launchGallery(View view){

        Boolean ch = checkPermissionREAD_EXTERNAL_STORAGE(this);
        if (ch) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent, "CHOOSE PICTURE"), REQUEST_IMAGE_SELECT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            try{
                if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                    mainPhoto = MediaStore.Images.Media.getBitmap(MainActivity.this.getContentResolver(), mainUri);
                    jinxPhoto = mainPhoto;

                    showMyDialog(MainActivity.this);
                    new CamTask().execute("");
                }

            } catch (Exception e){
                e.printStackTrace();
            }
        }

        if(requestCode == REQUEST_IMAGE_SELECT && resultCode == RESULT_OK){
            if (data == null) { return; }
            try {
                if (checkPermissionREAD_EXTERNAL_STORAGE(this)) {
                    inputStream = MainActivity.this.getContentResolver().openInputStream(data.getData());
                    String mCurrentPhotoPath = ImageFilePath.getPath(MainActivity.this, data.getData());

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                    options.inSampleSize = calculateInSampleSize(options, 240, 280);
                    options.inJustDecodeBounds = false;
                    jinxPhoto = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

                    showMyDialog(MainActivity.this);
                    new PostTask().execute("");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    private class CamTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            Uri tempUri = getBareImageUri(getApplicationContext(), mainPhoto);
            String mCurrentPhotoPath = getRealPathFromURI(tempUri);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, options);
            options.inSampleSize = calculateInSampleSize(options, 600, 700);
            options.inJustDecodeBounds = false;
            mainPhoto = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            return "DONE!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            ((SharedData) MainActivity.this.getApplication()).setSomeBitmap(mainPhoto);

            Intent intent = new Intent(MainActivity.this, EditActivity.class);
            MainActivity.this.startActivity(intent);
        }
    }

    private class PostTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            mainPhoto = BitmapFactory.decodeStream(inputStream);
            if(hasSecret(mainPhoto)){
                int strLength = Integer.parseInt(get_length(mainPhoto, 0));
                String hiddenMessage = decode_text(mainPhoto, strLength, 0);

                ((SharedData) MainActivity.this.getApplication()).setSomeVariable(hiddenMessage);
            }

            Uri tempUri = getBareImageUri(getApplicationContext(), mainPhoto);
            String mCurrentPhotoPath = getRealPathFromURI(tempUri);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mCurrentPhotoPath, options);
            options.inSampleSize = calculateInSampleSize(options, 600, 700);
            options.inJustDecodeBounds = false;
            mainPhoto = BitmapFactory.decodeFile(mCurrentPhotoPath, options);

            return "DONE!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            ((SharedData) MainActivity.this.getApplication()).setSomeBitmap(mainPhoto);

            Intent intent = new Intent(MainActivity.this, EditActivity.class);
            MainActivity.this.startActivity(intent);
        }
    }

    private void showMyDialog(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.display_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        ImageView imv3 = (ImageView) dialog.findViewById(R.id.imageView3);
        imv3.setImageBitmap(jinxPhoto);

        ProgressBar progreso;
        ObjectAnimator progressAnimator;
        progreso = (ProgressBar)dialog.findViewById(R.id.progressBar);
        progressAnimator = ObjectAnimator.ofInt(progreso, "progress", 0,100);
        progressAnimator.setDuration(5000);
        progressAnimator.start();

        /**
         * if you want the dialog to be specific size, do the following
         * this will cover 85% of the screen (85% width and 85% height)
         */
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dialogWidth = (int)(displayMetrics.widthPixels * 0.90);
        int dialogHeight = (int)(displayMetrics.heightPixels * 0.70);
        dialog.getWindow().setLayout(dialogWidth, dialogHeight);

        dialog.show();
    }

    public Uri getBareImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private boolean hasSecret(Bitmap imageFile){

        String lengthChk = get_length(imageFile, 0);
        int len;
        boolean pars;

        pars = isParsable(lengthChk);
        if(pars){
            len = Integer.parseInt(lengthChk);
            if(len > 0 && len < 1000){
                return true;
            }else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isParsable(String input){
        boolean parsable = true;
        try{
            Integer.parseInt(input);
        }catch(NumberFormatException e){
            parsable = false;
        }
        return parsable;
    }

    private String get_length(Bitmap imageFile, int offset){
        int x = 0, y = 0;
        Bitmap image = imageFile.copy(Bitmap.Config.ARGB_8888, true);
        String strLen = "";

        for(int i=0; i<3; ++i)
        {
            String strEnc = "";
            for(int bit=7; bit>=0; --bit, ++offset)
            {
                int rgbV = image.getPixel(x, y);

                int a = (rgbV >> 24) & 0xFF;
                int r = (rgbV >> 16) & 0xFF;
                int g = (rgbV >> 8) & 0xFF;
                int b = (rgbV >> 0) & 0xFF;

                int lsb = ((b % 2));
                strEnc += Integer.toString(lsb);

                if((y+1)< image.getHeight()){
                    y++;
                } else if ((x+1)< image.getWidth()){
                    x++;
                    y=0;
                } else {
                    System.out.println("Reached end of image file!");
                }

            }

            int charCode = Integer.parseInt(strEnc, 2);
            String str = new Character((char)charCode).toString();
            strLen += str;
        }

        return strLen;
    }

    private String decode_text(Bitmap imagefile, int lenns, int offset){
        int x = 0, y = 0;
        Bitmap image = imagefile.copy(Bitmap.Config.ARGB_8888, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] imageByteArray = stream.toByteArray();

        if(lenns + offset > imageByteArray.length)
        {
            throw new IllegalArgumentException("File not long enough!");
        }

        String manix = "";

        for(int i=0; i<lenns+3; ++i)
        {

            String street = "";
            for(int bit=7; bit>=0; --bit, ++offset)
            {

                int rgbV = image.getPixel(x, y);

                int a = (rgbV >> 24) & 0xFF;
                int r = (rgbV >> 16) & 0xFF;
                int g = (rgbV >> 8) & 0xFF;
                int b = (rgbV >> 0) & 0xFF;

                int lsb = ((b % 2));
                street += Integer.toString(lsb);


                if((y+1)< image.getHeight()){
                    y++;
                } else if ((x+1)< image.getWidth()){
                    x++;
                    y=0;
                } else {
                    System.out.println("EOLOOLEO1111");
                }

            }
            System.out.println("---STREET TEST");
            System.out.println(street);
            int charCode = Integer.parseInt(street, 2);
            String str = new Character((char)charCode).toString();
            System.out.println((str));
            System.out.println("---STREET TEST");

            manix += str;
        }

        System.out.println("EOLOOLEO2222");
        manix = manix.substring(3);
        return manix;
    }

    public Uri saveBaseImage(Bitmap nowPhoto, int x) {

        Bitmap icon = nowPhoto;
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        if (x == 0) {

            boolean bool = shouldAskPermission();
            if (bool) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }
        } else {

            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "Bumblebee/" + timeStamp + "base.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                icon.compress(Bitmap.CompressFormat.PNG, 100, fo);
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return Uri.parse("file:///sdcard/Bumblebee/"+timeStamp+"base.jpg");
    }

    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        if(permsRequestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                System.out.println("NOTE: Saving Image Locally!");
                saveBaseImage(mainPhoto, 1);

            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        (new AlertDialog.Builder(this))
                .setTitle("Exit Bumblebee")
                .setMessage("Do you want to exit?")
                .setPositiveButton("NO", null)
                .setNegativeButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAffinity();
                    }
                })
                .show();
    }

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context,
                            Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    public void showDialog(final String msg, final Context context,
                           final String permission) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(msg + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[] { permission },
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

}
