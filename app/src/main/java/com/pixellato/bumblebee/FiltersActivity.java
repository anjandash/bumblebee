package com.pixellato.bumblebee;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;
import android.net.Uri;
import android.opengl.GLException;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class FiltersActivity extends AppCompatActivity implements GLSurfaceView.Renderer {

    Bitmap mainPhoto;
    Uri mainUri;
    int clickCount = 0;
    boolean changed = false;

    private GLSurfaceView mEffectView;
    private int[] mTextures = new int[2];
    private EffectContext mEffectContext;
    private Effect mEffect;
    private TextureRender mTexRenderer = new TextureRender();
    private int mImageWidth;
    private int mImageHeight;
    private boolean mInitialized = false;
    int mCurrentEffect;
    private volatile boolean saveFrame;

    int totalWidth, totalHeight, pixWidth, pixHeight;
    int actualHeight, actualWidth;

    public void setCurrentEffect(int effect) {
        mCurrentEffect = effect;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);


        clickCount = 0;

        // get info from previous activity
        Bitmap bitmap = ((SharedData) FiltersActivity.this.getApplication()).getSomeBitmap();


        pixWidth = bitmap.getWidth();
        pixHeight = bitmap.getHeight();

        mainPhoto = bitmap;
        mainUri = getBareImageUri(FiltersActivity.this, bitmap);

        mEffectView = (GLSurfaceView) findViewById(R.id.effectsview);
        mEffectView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        mEffectView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
        mEffectView.setZOrderOnTop(true);

        mEffectView.setEGLContextClientVersion(2);
        mEffectView.setRenderer(this);
        mEffectView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        mCurrentEffect = R.id.none;

        int [] filters =  new int[]{
                R.id.none,
                R.id.vignette,
                R.id.brightness,
                R.id.crossprocess,
                R.id.duotone,
                R.id.grain,
                R.id.grayscale,
                R.id.lomoish,
                R.id.negative,
                R.id.posterize,
                R.id.saturate,
                R.id.sepia
        };

        int [] drawables =  new int[]{
                R.drawable.none,
                R.drawable.vignette,
                R.drawable.brightness,
                R.drawable.crossprocess,
                R.drawable.duotone,
                R.drawable.grain,
                R.drawable.grayscale,
                R.drawable.lomoish,
                R.drawable.negative,
                R.drawable.posterize,
                R.drawable.saturate,
                R.drawable.sepia
        };

        for (int i=0; i< filters.length; i++){
            int filterId = filters[i];
            ImageView im = (ImageView) findViewById(filterId);

            int drawableId = drawables[i];
            Bitmap conv = BitmapFactory.decodeResource(getResources(), drawableId);
            im.setImageBitmap(conv);

        }

    }

    private void loadTextures() {
        // Generate textures
        GLES20.glGenTextures(2, mTextures, 0);

        Bitmap bitmap = mainPhoto;
        mImageWidth = bitmap.getWidth();
        mImageHeight = bitmap.getHeight();
        mTexRenderer.updateTextureSize(mImageWidth, mImageHeight);

        // Upload to texture
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

        // Set texture parameters
        GLToolbox.initTexParams();
    }


    private void initEffect() {
        EffectFactory effectFactory = mEffectContext.getFactory();
        if (mEffect != null) {
            mEffect.release();
        }

        switch (mCurrentEffect) {
            case R.id.none:
                break;
            case R.id.vignette:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_VIGNETTE);
                mEffect.setParameter("scale", .5f);
                break;
            case R.id.brightness:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_BRIGHTNESS);
                mEffect.setParameter("brightness", 2.0f);
                break;
            case R.id.crossprocess:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_CROSSPROCESS);
                break;
            case R.id.duotone:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_DUOTONE);
                mEffect.setParameter("first_color", Color.YELLOW);
                mEffect.setParameter("second_color", Color.DKGRAY);
                break;
            case R.id.grain:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_GRAIN);
                mEffect.setParameter("strength", 1.0f);
                break;
            case R.id.grayscale:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_GRAYSCALE);
                break;
            case R.id.lomoish:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_LOMOISH);
                break;
            case R.id.negative:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_NEGATIVE);
                break;
            case R.id.posterize:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_POSTERIZE);
                break;
            case R.id.saturate:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_SATURATE);
                mEffect.setParameter("scale", .5f);
                break;
            case R.id.sepia:
                mEffect = effectFactory.createEffect(
                        EffectFactory.EFFECT_SEPIA);
                break;
            default:
                break;
        }
    }

    private void applyEffect() {
        mEffect.apply(mTextures[0], mImageWidth, mImageHeight, mTextures[1]);
    }

    private void renderResult() {
        if (mCurrentEffect != R.id.none) {
            // if no effect is chosen, just render the original bitmap
            mTexRenderer.renderTexture(mTextures[1]);
        }
        else {
            saveFrame=true;
            // render the result of applyEffect()
            mTexRenderer.renderTexture(mTextures[0]);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if (!mInitialized) {
            //Only need to do this once
            mEffectContext = EffectContext.createWithCurrentGlContext();
            mTexRenderer.init();
            loadTextures();
            mInitialized = true;
        }
        if (mCurrentEffect != R.id.none) {
            //if an effect is chosen initialize it and apply it to the texture
            changed = true;
            initEffect();
            applyEffect();
        }
        renderResult();
        if (saveFrame) {
            int adjustedWidth, adjustedHeight;
            totalWidth = mEffectView.getWidth();
            totalHeight = mEffectView.getHeight();

            if (totalHeight * pixWidth <= totalWidth * pixHeight) {
                actualWidth = pixWidth * totalHeight / pixHeight;
                actualHeight = totalHeight;
                adjustedWidth = Math.abs(totalWidth - actualWidth) / 2;
                adjustedHeight = 0;
            } else {
                actualHeight = pixHeight * totalWidth / pixWidth;
                actualWidth = totalWidth;
                adjustedHeight = Math.abs(totalHeight - actualHeight) / 2;
                adjustedWidth = 0;
            }

            mainPhoto = createBitmapFromGLSurface(adjustedWidth,adjustedHeight,actualWidth,actualHeight, gl);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if (mTexRenderer != null) {
            mTexRenderer.updateViewSize(width, height);
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }


    public boolean handleClick(View view){
        setCurrentEffect(view.getId());
        mEffectView.requestRender();
        return true;
    }

    public void handleFiltersDone(View view){
        ((SharedData) FiltersActivity.this.getApplication()).setSomeBitmap(mainPhoto);
        Intent myIntent = new Intent(FiltersActivity.this, EncryptionActivity.class);
        FiltersActivity.this.startActivity(myIntent);
    }

    public Uri getBareImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private Bitmap createBitmapFromGLSurface(int x, int y, int w, int h, GL10 gl)throws OutOfMemoryError {
        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            gl.glReadPixels(x, y, w, h, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            return null;
        }

        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    public Uri saveImage(Bitmap nowPhoto, int x) {

        Bitmap icon = nowPhoto;
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        if (x == 0) {

            boolean bool = shouldAskPermission();
            if (bool) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }
        } else {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Bumble");
            myDir.mkdirs();

            String fname = timeStamp + "-flt-saved.jpg";
            File f = new File (myDir, fname);
            if (f.exists ()) f.delete ();

            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                icon.compress(Bitmap.CompressFormat.PNG, 100, fo);
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return Uri.parse("file:///sdcard/Bumble/"+timeStamp+"-flt-saved.jpg");
    }

    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        if(permsRequestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                System.out.println("NOTE: Saving Image Locally!");
                saveImage(mainPhoto, 1);

            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onBackPressed() {

        Intent myIntent = new Intent(FiltersActivity.this, EditActivity.class);
        FiltersActivity.this.startActivity(myIntent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        String hiddenMsg = ((SharedData) FiltersActivity.this.getApplication()).getSomeVariable();
        if(hiddenMsg != null){
            MenuItem alertButton = menu.findItem(R.id.action_alert);
            alertButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_alert) {

            String hiddenMsg = ((SharedData) FiltersActivity.this.getApplication()).getSomeVariable();
            clickCount++;
            if(true){
                Toast toast=Toast.makeText(getApplicationContext(), hiddenMsg, Toast.LENGTH_SHORT);
                toast.show();
                clickCount = 0;
            }
            return true;
        }
        if (id == R.id.action_save) {

            Toast toast=Toast.makeText(getApplicationContext(),"Saving...",Toast.LENGTH_SHORT);
            toast.show();

            saveImage(mainPhoto, 1);
            toast.cancel();

            Toast toastSaved=Toast.makeText(getApplicationContext(),"Image saved in folder /Bumble.",Toast.LENGTH_SHORT);
            toastSaved.show();
            return true;

        }

        if (id == R.id.action_cancel) {

            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
