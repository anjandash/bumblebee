package com.pixellato.bumblebee;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.content.Context;
import android.app.Dialog;
import android.util.DisplayMetrics;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class EncryptionActivity extends AppCompatActivity {

    Bitmap mainPhoto, adjustedBitmap;
    Uri mainUri;
    int clickCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encryption);

        clickCount = 0;
        Bitmap bitmap;
        Uri myUri;

        bitmap = ((SharedData) EncryptionActivity.this.getApplication()).getSomeBitmap();
        myUri =  getBareImageUri(EncryptionActivity.this, bitmap);

        adjustedBitmap = bitmap;
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageBitmap(adjustedBitmap);
        mainPhoto = adjustedBitmap;
        mainUri = myUri;
    }

    public void gotoEncrypt(View view){
        Context contextx = EncryptionActivity.this;
        showMyTextDialog(contextx);
    }

    public Uri getBareImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void showMyTextDialog(Context context) {
        final Dialog dialog2 = new Dialog(context);
        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog2.setContentView(R.layout.text_dialog);
        dialog2.setCanceledOnTouchOutside(false);
        dialog2.setCancelable(true);

        Button btnBtmBack = (Button) dialog2.findViewById(R.id.btnBtmBack);
        Button btnBtmEncrypt = (Button) dialog2.findViewById(R.id.btnBtmEncrypt);

        btnBtmBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });

        btnBtmEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // encrypt the text
                View view = EncryptionActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                TextView editText = (TextView) dialog2.findViewById(R.id.editText);
                String a = editText.getText().toString();

                Context contextx2 = EncryptionActivity.this;
                showMyProgressDialog(contextx2, null, "");

                Bitmap newImage = encodeThis(mainPhoto, a);


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("DELAY CHK");
                    }
                }, 1000);
                dialog2.dismiss();

                if(newImage == null){
                    System.out.println("ERR: New Image returned is null!");
                } else {

                    // store image and get uri
                    Uri myUri = saveImage(newImage, 1);
                    ((SharedData) EncryptionActivity.this.getApplication()).setSomeBitmap(newImage);

                    // pass the uri
                    Intent intent = new Intent(EncryptionActivity.this, ShareActivity.class);
                    intent.putExtra("imageUri", myUri.toString());
                    EncryptionActivity.this.startActivity(intent);
                }
            }
        });

        /**
         * if you want the dialog to be specific size, do the following
         * this will cover 85% of the screen (85% width and 85% height)
         */
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dialogWidth = (int)(displayMetrics.widthPixels * 0.85);
        int dialogHeight = (int)(displayMetrics.heightPixels * 0.45);
        dialog2.getWindow().setLayout(dialogWidth, dialogHeight);

        dialog2.show();
    }

    private void showMyProgressDialog(Context context, Bitmap photo, String a) {
        final Dialog dialog23 = new Dialog(context);
        dialog23.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog23.setContentView(R.layout.progress_dialog);
        dialog23.setCanceledOnTouchOutside(false);
        dialog23.setCancelable(true);

        /**
         * if you want the dialog to be specific size, do the following
         * this will cover 85% of the screen (85% width and 85% height)
         */
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dialogWidth = (int)(displayMetrics.widthPixels * 0.90);
        int dialogHeight = (int)(displayMetrics.heightPixels * 0.20);
        dialog23.getWindow().setLayout(dialogWidth, dialogHeight);

        dialog23.show();

    }

    public Bitmap encodeThis(Bitmap photo, String a){
        byte[] origMsgBytes = a.getBytes();
        int origMsgLen = origMsgBytes.length;
        String strMsgLen = Integer.toString(origMsgLen);

        if(strMsgLen.length() == 1){
            a = "00"+strMsgLen+a;
        } else if(strMsgLen.length() == 2){
            a = "0"+strMsgLen+a;
        } else if(strMsgLen.length() == 3){
            a = strMsgLen+a;
        }

        byte[] add = a.getBytes();
        Bitmap finImage = encode_text(photo, add);

        return finImage;
    }

    private void showMySecretDialog(Context context, String secret){
        final Dialog dialogsx = new Dialog(context);
        dialogsx.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogsx.setContentView(R.layout.secret_dialog);
        dialogsx.setCanceledOnTouchOutside(false);
        dialogsx.setCancelable(true);

        TextView textView = (TextView) dialogsx.findViewById(R.id.secretTxt);
        Button btnDismiss = (Button) dialogsx.findViewById(R.id.btnDismiss);

        textView.setTextColor(Color.parseColor("#ff4081"));
        textView.setText(secret);

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogsx.dismiss();
            }
        });

        /**
         * if you want the dialog to be specific size, do the following
         * this will cover 85% of the screen (85% width and 85% height)
         */
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dialogWidth = (int)(displayMetrics.widthPixels * 0.95);
        int dialogHeight = (int)(displayMetrics.heightPixels * 0.60);
        dialogsx.getWindow().setLayout(dialogWidth, dialogHeight);

        dialogsx.show();
    }

    private Bitmap encode_text(Bitmap imageFile, byte[] addition) {
        int x = 0, y = 0;
        Bitmap image = imageFile.copy(Bitmap.Config.ARGB_8888, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] imageByteArray = stream.toByteArray();

        if(addition.length > imageByteArray.length)
        {
            Toast toast = Toast.makeText(getApplicationContext(), "Sorry! Your text is too long.", Toast.LENGTH_SHORT);
            toast.show();
            throw new IllegalArgumentException("File not long enough!");
        }


        for(int i=0; i<addition.length; ++i)
        {
            int add = addition[i];
            for(int bit=7; bit>=0; --bit)
            {

                int changeBit = (add >>> bit) & 1;
                System.out.print(changeBit);

                int rgbV = image.getPixel(x, y);

                int a = (rgbV >> 24) & 0xFF;
                int r = (rgbV >> 16) & 0xFF;
                int g = (rgbV >> 8) & 0xFF;
                int b = (rgbV >> 0) & 0xFF;

                b = ((b & 0xFE) | changeBit);

                int rgb = a;
                rgb = rgb << 8;
                rgb |= r;
                rgb = rgb << 8;
                rgb |= g;
                rgb = rgb << 8;
                rgb |= b;

                try {
                    image.setPixel(x, y, rgb);
                }catch (Exception e){
                    System.out.println("setPixel EXCEPTION: " + e);
                }

                if((y+1)< image.getHeight()){
                    y++;
                } else if ((x+1)< image.getWidth()){
                    x++;
                    y=0;
                } else {
                    return image;
                }
            }
        }

        return image;
    }


    public Uri saveImage(Bitmap nowPhoto, int x) {

        Bitmap icon = nowPhoto;
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

        if (x == 0) {
            boolean bool = shouldAskPermission();
            if (bool) {
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }

        } else {

            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/Bumble");
            myDir.mkdirs();

            String fname = timeStamp + "-encr-saved.jpg";
            File f = new File (myDir, fname);
            if (f.exists ()) f.delete ();

            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                icon.compress(Bitmap.CompressFormat.PNG, 100, fo);
                fo.flush();
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        return Uri.parse("file:///sdcard/Bumble/"+timeStamp+"-encr-saved.jpg");
    }

    private boolean shouldAskPermission(){
        return(Build.VERSION.SDK_INT>Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults){

        if(permsRequestCode == 200){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // save file
                System.out.println("NOTE: Saving Image Locally!");
                saveImage(mainPhoto, 1);

            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        String hiddenMsg = ((SharedData) EncryptionActivity.this.getApplication()).getSomeVariable();

        if(hiddenMsg != null){
            MenuItem alertButton = menu.findItem(R.id.action_alert);
            alertButton.setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_alert) {

            String hiddenMsg = ((SharedData) EncryptionActivity.this.getApplication()).getSomeVariable();
            clickCount++;
            if(true){
                Toast toast=Toast.makeText(getApplicationContext(), hiddenMsg, Toast.LENGTH_SHORT);
                toast.show();
                clickCount = 0;
            }
            return true;
        }
        if (id == R.id.action_save) {

            Toast toast=Toast.makeText(getApplicationContext(),"Saving...",Toast.LENGTH_SHORT);
            toast.show();

            saveImage(mainPhoto, 1);
            toast.cancel();

            Toast toastSaved=Toast.makeText(getApplicationContext(),"Image saved in folder /Bumble.",Toast.LENGTH_SHORT);
            toastSaved.show();
            return true;
        }

        if (id == R.id.action_cancel) {

            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EncryptionActivity.this, FiltersActivity.class);
        intent.putExtra("sourceAct", "FiltersActivity");
        intent.putExtra("imageUri", mainUri.toString());
        EncryptionActivity.this.startActivity(intent);
    }

}
