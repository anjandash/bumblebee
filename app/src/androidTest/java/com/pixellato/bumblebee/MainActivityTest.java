package com.pixellato.bumblebee;

import android.app.Activity;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anjan on 1/28/2018.
 */
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    private MainActivity mainActivity = null;

    @Before
    public void setUp() throws Exception {
        mainActivity = mainActivityActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch(){
        View gButton = mainActivity.findViewById(R.id.galleryButton);
        assertNotNull(gButton);

        View pButton = mainActivity.findViewById(R.id.photoButton);
        assertNotNull(pButton);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}